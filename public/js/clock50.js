$(document).ready(function () {
	setTimeout(function () {
		init();
		setCss();
		set_triangle();
		setSolarGradient();
		initMic();
		$(window).resize(function () {
				init();
				setCss();
				set_triangle();
				setSolarGradient();
			}
		);
		fullscreen_toggle_by_enter_key();
		fullscreen_toggle_by_double_click();
		coords();
		solarLineCalc();
	}, 500);
	setInterval(time, 1000);
	volume_test();
})

function solarLineCalc() {
	let events = [
		'nadir', //надир (самый темный момент ночи, солнце находится в самом низком положении)
		'nightEnd', //заканчивается ночь (начинаются утренние астрономические сумерки)
		'nauticalDawn', //морской рассвет (начинаются утренние морские сумерки)
		'dawn', //рассвет (заканчиваются утренние морские сумерки, начинаются утренние гражданские сумерки)
		'sunrise', //восход солнца (верхний край солнца появляется на горизонте)
		'sunriseEnd', //восход солнца заканчивается (нижний край солнца касается горизонта)
		'goldenHourEnd', //золотой час утра (мягкий свет, лучшее время для фотосъемки) заканчивается
		'solarNoon', //солнечный полдень (солнце находится в самом высоком положении)
		'goldenHour', //Золотой час вечера начинается
		'sunsetStart', //начинается закат (нижний край солнца касается горизонта)
		'sunset', //закат (солнце уходит за горизонт, начинаются вечерние гражданские сумерки)
		'dusk', //сумерки (начинаются вечерние морские сумерки)
		'nauticalDusk', //морские сумерки (начинаются вечерние астрономические сумерки)
		'night', //начинается ночь (достаточно темно для астрономических наблюдений)
	],
		periods = [

		],
		// times = SunCalc.getTimes(new Date('December 22'), 71.639263, 128.870855), //Тикси
		times = SunCalc.getTimes(new Date(), $.cookie('latitude'), $.cookie('longitude')),
		out = {};
	$.each(events, function (index, event) {
		event = event.replace()
		out[event] = getSecondFromBegin(times, event);
	});
	console.log(out);
	return out;
}

function nanToZero(value) {
	return isNaN(value)?0:value;
}

function coords() {
	navigator.geolocation.getCurrentPosition((position) => {
		$.cookie('latitude', position.coords.latitude, { expires: 7, path: '/' });
		$.cookie('longitude', position.coords.longitude, { expires: 7, path: '/' });
	});
}

function getSecondFromBegin(times, event) {
	let eventTime = new Date(times[event]);
	return eventTime.getHours()*3600 + eventTime.getMinutes()*60;
}
let conf = {},
	screen_height,
	screen_width;

function init() {
	conf['triangle_height'] = 0.03;
	conf['triangle_half_width'] = conf.triangle_height/2;
	conf['nightColor'] = '#08076e';
	conf['dayColor'] = '#faf205';
	conf['sunColor'] = '#fa8805';
	conf['daylightColor'] = '#05b9fa';
	screen_height = document.documentElement.clientHeight;
	screen_width = document.documentElement.clientWidth;
}

function setCss() {
	$('#total_wrap').css({
		height: screen_height+'px'
	});
	$('#clock_value').css({
		"font-size": screen_height*0.4+'px',
		width: screen_height*0.3*150/41+'px',
		"padding-top": screen_height*0.01*135/9+'px'
	});
	$('#second').css({
		"font-size": screen_height*0.12+'px',
	});
}

function set_triangle() {
	$('#sun_triangle_indicator').css({
		"border-left": screen_height*conf.triangle_half_width + "px solid transparent",
		"border-right": screen_height*conf.triangle_half_width + "px solid transparent",
		"border-top": screen_height*conf.triangle_height + "px solid #adff2f",
		"margin-left": (getTrianglePosition(screen_width, screen_height)) + "px",
	});
	$('#sun_time_slash').css({
		"position": "absolute",
		"left": (getTrianglePosition(screen_width, screen_height)-1+screen_height*conf.triangle_half_width) + "px",
	})
}

function setSolarGradient() {
	let solar_data = solarLineCalc(),
		night0 = solar_data.dawn/86400*screen_width+'px',
		daylight0 = (solar_data.sunrise - solar_data.dawn)/86400*screen_width+'px',
		sunrise = (solar_data.sunriseEnd - solar_data.sunrise)/86400*screen_width+'px',
		day = (solar_data.sunsetStart - solar_data.sunriseEnd)/86400*screen_width+'px',
		sunset = (solar_data.sunset - solar_data.sunsetStart)/86400*screen_width+'px',
		daylight1 = (solar_data.dusk - solar_data.sunset)/86400*screen_width+'px',
		night1 = (86400 - solar_data.dusk)/86400*screen_width+'px';
	$('#night0').css({
		"width": night0,
		"background-color": conf.nightColor
	});
	$('#daylight0').css({
		"width": daylight0,
		"background-color": conf.daylightColor
	});
	$('#sunrise').css({
		"width": sunrise,
		"background-color": conf.sunColor
	});
	$('#day').css({
		"width": day,
		"background-color": conf.dayColor
	});
	$('#sunset').css({
		"width": sunset,
		"background-color": conf.sunColor
	});
	$('#daylight1').css({
		"width": daylight1,
		"background-color": conf.daylightColor
	});
	$('#night1').css({
		"width": night1,
		"background-color": conf.nightColor
	});
}

function getTrianglePosition(screen_width, screen_height) {
	let date = new Date(),
		second_from_day_beginning,
		current_timestamp = date.getTime(),
		begin_timestamp;
	date.setHours(0, 0, 0, 0);
	begin_timestamp = date.getTime();
	second_from_day_beginning = Math.round((current_timestamp - begin_timestamp)/1000);
	return Math.round(second_from_day_beginning/86400*screen_width - screen_height*conf.triangle_half_width);
}

function initMic() {
	$('#total_wrap').click();
}

function time() {
	let d = new Date();
	let s = d.getSeconds(),
		m = d.getMinutes(),
		h = d.getHours(),
		delimiter = '<span id="delimiter">:</span>',
		minutes, hours, time_string, time_for_title, second;

	time_string = ("0"+h).substr(-2) + delimiter + ("0"+m).substr(-2);
	time_for_title = ("0"+h).substr(-2) + ':' + ("0"+m).substr(-2);
	second = ("0"+s).substr(-2);
	$('#second').html(second);
	$('#clock_value').html(time_string);
	if (parseInt(s)%2 === 0) {
		$('#delimiter').addClass('delimiter_visible');
	}
	else {
		$('#delimiter').removeClass('delimiter_visible');
	}
	$(document).prop('title', time_for_title);
	set_triangle();
	if (s===0 && m===0) {
		let ding_count = (h === 0)?24:h;
		ding_count = (ding_count > 12)?(ding_count - 12):ding_count;
		let ding_element_id = 'ding' + ding_count;
		let ding_sound = document.getElementById(ding_element_id);
		ding_sound.play();
	}
	if(s===0 && (m===15 || m===30 || m===45)) {
		let navy_element_id = 'navy' + m;
		let navy_sound = document.getElementById(navy_element_id);
		navy_sound.play();
	}
}

function toggle_fullscreen() {
	if (document.fullscreenElement) {
		document.exitFullscreen()
			.then(() => {})
			.catch((err) => console.error(err));
	} else {
		document.documentElement
			.requestFullscreen()
			.then(() => {})
			.catch((err) => console.error(err));
	}
}

function fullscreen_toggle_by_enter_key() {
	document.addEventListener("keydown", function(e) {
		if (e.key === 'Enter') {
			toggle_fullscreen();
		}
	}, false);
}

function fullscreen_toggle_by_double_click() {
	$('#total_wrap').dblclick(function () {
		toggle_fullscreen();
		clearSelection();
	});
}

function volume_test() {
	$('#volume_test').click(function () {
		document.getElementById('navy45').play();
		return false;
	})
}

function clearSelection() {
	if (window.getSelection) {
		window.getSelection().removeAllRanges();
	}
}




